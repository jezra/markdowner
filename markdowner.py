#!/usr/bin/env python
'''
Hey, this is GPLv3 and copyright 2012 Jezra
and Jon Staley
'''

import os, sys

#Gtk
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

#webkit
gi.require_version('WebKit2', '4.0')
from gi.repository import WebKit2 as webkit

from gi.repository import Gdk 

import markdown


try:
  import gtkspell
  has_gtkspell=True
except:
  has_gtkspell=False

class application:
  current_file = None
  current_html_file = None
  title = "markdowner"
  local_dir = os.path.dirname(__file__)
  #hey package maintainers, change the next line to point to the directory with the icon
  icon_dir = local_dir

  def __init__(self,initial_file=None, markdown_while_typing=False):
    #build the UI
    self.winder = Gtk.Window(Gtk.WindowType.TOPLEVEL)
    self.winder.connect("destroy",self.quit)
    self.winder.maximize()
    #set the icon
    icon = os.path.join(self.icon_dir, 'icon.png')
    self.winder.set_default_icon_from_file(icon)
    #add some accellerators
    #create an accellerator group for this window
    accel = Gtk.AccelGroup()
    #add the ctrl+m to open
    accel.connect(Gdk.KEY_M, Gdk.ModifierType.CONTROL_MASK, Gtk.AccelFlags.VISIBLE, self.accel_markdown )
    #add the ctrl+o to open
    accel.connect(Gdk.KEY_O, Gdk.ModifierType.CONTROL_MASK, Gtk.AccelFlags.VISIBLE, self.accel_open )
    #add the ctrl+r to reload
    accel.connect(Gdk.KEY_R, Gdk.ModifierType.CONTROL_MASK, Gtk.AccelFlags.VISIBLE, self.accel_reload )
    #add the ctrl+q to quit
    accel.connect(Gdk.KEY_Q, Gdk.ModifierType.CONTROL_MASK, Gtk.AccelFlags.VISIBLE, self.accel_quit )
    #add ctrl+s to save
    accel.connect(Gdk.KEY_S, Gdk.ModifierType.CONTROL_MASK, Gtk.AccelFlags.VISIBLE, self.accel_save )
    #add ctrl+shift+s to save
    accel.connect(Gdk.KEY_S, Gdk.ModifierType.CONTROL_MASK|Gdk.ModifierType.SHIFT_MASK, Gtk.AccelFlags.VISIBLE, self.accel_save_as )
    #lock the group
    accel.lock()
    #add the group to the window
    self.winder.add_accel_group(accel)
    #we need a paned
    pane = Gtk.HPaned()
    #and a vbox
    box = Gtk.VBox(False)
    #add the pane to the box
    box.pack_start(pane, True, True, 0)
    #add the box to the window
    self.winder.add(box)
    #do the text crap for the first pane
    self.tb = Gtk.TextBuffer()
    self.tv = Gtk.TextView.new_with_buffer(self.tb)
    self.tv.set_wrap_mode(Gtk.WrapMode.WORD_CHAR)
    self.tv.connect("key_release_event", self.on_type)
    #try and add spell checking to the textview
    if has_gtkspell:
      #what if there is no aspell library?
      try:
        self.spell = gtkspell.Spell(self.tv)
        self.has_spell_library=True
      except Exception:
        #bummer
        self.has_spell_library=False
        print( Exception.message)
    #add the text view to a scrollable window
    input_scroll = Gtk.ScrolledWindow()
    input_scroll.add(self.tv)
    input_scroll.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC)
    #add to the pane
    pane.pack1(input_scroll,True)
    #make the output notebook
    outnb = Gtk.Notebook()
    pane.add2(outnb)
    #make the HTML viewable area
    self.wv = webkit.WebView()
    #disable the plugins for the webview
    ws = self.wv.get_settings()
    ws.set_property('enable-plugins',False)
    self.wv.set_settings(ws)
    out_scroll = Gtk.ScrolledWindow()
    out_scroll.add(self.wv)
    out_scroll.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC)
    outnb.append_page(out_scroll, Gtk.Label("Web View"))
    #make the html source textview for the output notebook
    self.outtb = Gtk.TextBuffer()
    outtv = Gtk.TextView.new_with_buffer(self.outtb)
    outtv.set_editable(False)
    outtv_scroll = Gtk.ScrolledWindow()
    outtv_scroll.add(outtv)
    outtv_scroll.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC)
    outnb.append_page(outtv_scroll, Gtk.Label("Source View"))
    #we will add buttons at the bottom
    bbox = Gtk.HBox()
    #we need buttons!
    savebutton = Gtk.Button("Save")
    savebutton.connect("clicked", self.on_save)
    saveasbutton = Gtk.Button("Save as")
    saveasbutton.connect("clicked", self.on_save_as)
    openbutton = Gtk.Button("Open")
    openbutton.connect("clicked", self.on_open)
    reloadbutton = Gtk.Button("Reload")
    reloadbutton.connect("clicked", self.on_reload)
    markdownbutton = Gtk.Button("Markdown")
    markdownbutton.connect("clicked", self.on_markdown)
    self.markdowncheckbutton = Gtk.CheckButton("Markdown while typing")
    #we also need to save the html
    savehtmlbutton = Gtk.Button("Save HTML")
    savehtmlbutton.connect("clicked", self.on_save_html)
    savehtmlasbutton = Gtk.Button("Save HTML as")
    savehtmlasbutton.connect("clicked", self.on_save_html_as)

    #add the buttons to the bbox
    bbox.pack_start(openbutton,False,False,0)
    bbox.pack_start(savebutton,False,False,0)
    bbox.pack_start(saveasbutton,False,False,0)
    bbox.pack_start(markdownbutton,False,False,0)
    bbox.pack_start(reloadbutton,False,False,0)
    bbox.pack_start(savehtmlbutton,False,False,0)
    bbox.pack_start(savehtmlasbutton,False,False,0)

    bbox.pack_start(self.markdowncheckbutton,False,False,0)
    #add the bbox to the box
    box.pack_start(bbox,False,False,0)
    self.winder.show_all()
    self.update_title()
    #is there an initial file?
    if initial_file:
      self.load_file(initial_file)
    #should we be marking down while typing?
    if markdown_while_typing:
      self.markdowncheckbutton.set_active(True)

  def update_title(self,string=None):
    title = self.title
    if string:
      title += " : "+string
    self.winder.set_title(title)

  def run(self):
    Gtk.main()

  def quit(self,widget=None):
    Gtk.main_quit()

  def on_reload(self, widget=None):
    if self.current_file:
      self.load_file(self.current_file)

  def on_open(self, widget=None):
    dialog = Gtk.FileChooserDialog(title="Select a file",
      action=Gtk.FileChooserAction.OPEN,
      buttons=(Gtk.STOCK_CANCEL,
      Gtk.ResponseType.CANCEL,
      Gtk.STOCK_OPEN,
      Gtk.ResponseType.OK))
    md_filter = Gtk.FileFilter()
    md_filter.set_name('MarkDown files')
    md_filter.add_pattern('*.md')
    txt_filter = Gtk.FileFilter()
    txt_filter.set_name('Text files')
    txt_filter.add_pattern('*.txt')
    all_filter = Gtk.FileFilter()
    all_filter.set_name('All files')
    all_filter.add_pattern('*')
    dialog.add_filter(md_filter)
    dialog.add_filter(txt_filter)
    dialog.add_filter(all_filter)
    response = dialog.run()

    if response == Gtk.ResponseType.OK:
      self.load_file( dialog.get_filename() )
    dialog.destroy()

  def on_save_as(self, widget=None):
    dialog = Gtk.FileChooserDialog(title="Save file as ...",
      action=Gtk.FileChooserAction.SAVE,
      buttons=(Gtk.STOCK_CANCEL,
      Gtk.ResponseType.CANCEL,
      Gtk.STOCK_SAVE,
      Gtk.ResponseType.OK))
    response = dialog.run()

    if response == Gtk.ResponseType.OK:
      self.current_file = dialog.get_filename()
      self.update_title(self.current_file)
      self.save(self.current_file)

    dialog.destroy()

  def on_save_html_as(self, widget=None):
    dialog = Gtk.FileChooserDialog(title="Save file as ...",
      action=Gtk.FileChooserAction.SAVE,
      buttons=(Gtk.STOCK_CANCEL,
      Gtk.ResponseType.CANCEL,
      Gtk.STOCK_SAVE,
      Gtk.ResponseType.OK))
    response = dialog.run()

    if response == Gtk.ResponseType.OK:
      self.current_html_file = dialog.get_filename()
      self.update_title(self.current_file)
      self.save_html(self.current_html_file)

    dialog.destroy()

  def on_save(self, widget=None):
    if self.current_file:
      try:
        self.save(self.current_file)
      except:
        self.on_save_as()
    else:
      self.on_save_as()

  def on_save_html(self, widget=None):
    if self.current_html_file:
      try:
        self.save_html(self.current_html_file)
      except:
        self.on_save_html_as()
    else:
      self.on_save_html_as()

  def load_file(self, filename):
    self.current_file = filename
    self.update_title(self.current_file)
    try:
      fh = open(filename, "r")
      text = fh.read()
      self.tb.set_text(text)
      fh.close()
      self.on_markdown()
    except IOError as err:
      print (err)

  def on_type(self, widget, data):
    if self.markdowncheckbutton.get_active():
      self.on_markdown()

  def on_markdown(self,widget=None):
    text = self.get_buffer_text()
    mdtext = markdown.markdown(text)
    self.wv.load_html(mdtext,"file:///")
    self.outtb.set_text(mdtext)

  def accel_quit(self,accel_group, acceleratable, keyval, modifier):
    self.quit()

  def accel_markdown(self,accel_group, acceleratable, keyval, modifier):
    self.on_markdown()

  def accel_save(self,accel_group, acceleratable, keyval, modifier):
    self.on_save()

  def accel_save_as(self,accel_group, acceleratable, keyval, modifier):
    self.on_save_as()

  def accel_open(self,accel_group, acceleratable, keyval, modifier):
    self.on_open()

  def accel_reload(self,accel_group, acceleratable, keyval, modifier):
    self.on_reload()

  def save(self,filename):
    if filename:
      #get the text
      text = self.get_buffer_text()
      f = open(filename,"w")
      f.write(text)
      f.close()
    else:
      pass

  def save_html(self,filename):
    if filename:
      #get the HTML text
      text = self.get_buffer_text()
      mdtext = markdown.markdown(text)
      f = open(filename,"w")
      f.write(mdtext)
      f.close()
    else:
      pass

  def get_buffer_text(self):
    start_iter = self.tb.get_start_iter()
    end_iter = self.tb.get_end_iter()
    text=self.tb.get_text(start_iter,end_iter,True)
    return text

if __name__=="__main__":
  initial_file = None
  markdown_while_typing = False
  if len(sys.argv) > 1:

    if sys.argv[1] == "-m":
      markdown_while_typing = True
      if len(sys.argv) > 2:
        initial_file = sys.argv[2]
    else:
      initial_file = sys.argv[1]

  print (sys.argv)
  a = application(initial_file, markdown_while_typing)
  a.run()

